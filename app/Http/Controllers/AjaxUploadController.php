<?php

namespace App\Http\Controllers;

use App\Ajax;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AjaxUploadController extends Controller
{
    function index()
    {
        $records = Ajax::orderBy('created_at','DESC')->paginate(5);
        return view('welcome',compact('records'));
    }

    function action(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        if($validation->passes())
        {
            $data = $request->all(); // This will get all the request data.
            $data['image'] = time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $data['image']);
            $record = Ajax::create($data);
            $builtRow = view('row',compact('record'))->render();
            return response()->json(['record'   => $builtRow]);
        }
        else
        {
            return response()->json([
                'message'   => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name'  => 'alert-danger'
            ]);
        }
    }

    function update(Request $request)
    {
//        return response()->json($request->all());
        $validation = Validator::make($request->all(), [
            'first_name' => 'sometimes',
            'last_name' => 'sometimes',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if($validation->passes())
        {
            $record = Ajax::find($request->id);
            if ($request->has('image')) {
                $data = $request->all(); // This will get all the request data.
                $data['image'] = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('images'), $data['image']);
            }
            $record->first_name = $request->first_name;
            $record->last_name = $request->last_name;
            $record->image = $data['image'];;
            $record->save();
            $builtRow = view('row',compact('record'))->render();
            return response()->json(['status' => 'success', 'record'   => $builtRow,'id' => $record->id]);

        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message'   => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name'  => 'alert-danger'
            ]);
        }
    }

    public function destroy($id){

        $record = Ajax::find($id);
        $old_image = 'images/' . $record->image;
        if (is_file($old_image)) unlink($old_image);
        $record->delete();
        return response()->json([
            'id' => $record->id
        ]);

    }
}
?>