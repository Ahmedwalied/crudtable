<tr id="tr{{$record->id}}">
    <td  style="vertical-align: middle;" class="text-center font-weight-bold" id="add_details">
        {{$record->id}}
    </td>
    <td style="vertical-align: middle;" class="text-center font-weight-bold" >
        <input type="text" value="{{$record->first_name}}" id="first_name_{{$record->id}}" class="first_name" name="fname" disabled>

    </td>
    <td style="vertical-align: middle;" class="text-center font-weight-bold" >
        <input type="text" value="{{$record->last_name}}" id="last_name_{{$record->id}}" class="last_name" name="lname" disabled >
    </td>
    <td style="vertical-align: middle;" class="text-center font-weight-bold" >
        <img src="/images/{{$record->image}}" class="img-thumbnail" width="100px" />
        <input type="hidden" class="file-input" id="image_{{$record->id}}">
    </td>

    <td style="vertical-align: middle;" class="text-center font-weight-bold">
        <a data-id="{{ $record->id }}" class="btn edit btn-primary">Edit</a>
        <input type="button" class="btn update_btn btn-success hidden" data-id="{{ $record->id }}" data-value="{{ $record->id }}" value="update"/>
        <a href="{{ route('ajaxupload.destroy',$record->id) }}" class="btn btn-danger deleteData" data-id="{{ $record->id }}" data-value="{{ $record->id }}">
            Delete
        </a>
    </td>
</tr>