<!DOCTYPE html>
<html>
<head>
    <title>Upload Image in Laravel using Ajax</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
<br />
<div class="container box">
    <h1>Add Some Data</h1>
    <div class="alert" id="message" style="display: none"></div>
    <form method="post" id="upload_form" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <table class="table table-striped table-bordered">
                <tr>
                    <th class="text-center form-group">
                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First name" required="">
                    </th>
                    <th class="text-center form-group">
                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" required="">
                    </th>
                    <th class="text-center form-group">
                        <input type="file" name="image" id="image" />
                    </th>
                    <th class="text-center form-group">
                        <input type="submit" name="upload" id="upload" class="btn btn-primary" value="Add">
                    </th>
                </tr>
            </table>
        </div>
    </form>
    <br />
            <h3 align="center">Data</h3><br/>
            <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <th class="text-center">
                                    <label for="id" >#</label>
                                </th>
                                <th class="text-center">
                                    <label for="last_name" >First Name</label>
                                </th>
                                <th class="text-center">
                                    <label for="last_name">Last Name</label>
                                </th>
                                <th class="text-center">
                                    <label for="image">Image</label>
                                </th>
                                <th class="text-center">
                                    <label for="add">Action</label>
                                </th>
                                </thead>
                                @foreach($records as $record)
                                    <tbody id="table_data">
                                    <tr id="tr{{$record->id}}">
                                        <td  style="vertical-align: middle;" class="text-center font-weight-bold" id="add_details">
                                            {{$record->id}}
                                        </td>
                                        <td style="vertical-align: middle;" class="text-center font-weight-bold" >
                                            <input type="text" value="{{$record->first_name}}" id="first_name_{{$record->id}}" class="first_name" name="fname" disabled>

                                        </td>
                                        <td style="vertical-align: middle;" class="text-center font-weight-bold" >
                                            <input type="text" value="{{$record->last_name}}" id="last_name_{{$record->id}}" class="last_name" name="lname" disabled >
                                        </td>
                                        <td style="vertical-align: middle;" class="text-center font-weight-bold" >
                                            <img src="/images/{{$record->image}}" class="img-thumbnail" width="100px" />
                                            <input type="hidden" class="file-input" id="image_{{$record->id}}">
                                        </td>

                                        <td style="vertical-align: middle;" class="text-center font-weight-bold">
                                            <a data-id="{{ $record->id }}" class="btn edit btn-primary">Edit</a>
                                            <input type="button" class="btn update_btn btn-success hidden" data-id="{{ $record->id }}" data-value="{{$record->id}}" value="update"/>
                                            <a href="{{ route('ajaxupload.destroy',$record->id) }}" class="btn btn-danger deleteData" data-id="{{ $record->id }}" data-value="{{ $record->id }}">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                            @endforeach
                        </table>
                            <div class="clearfix">
                                <div class="hint-text">Showing <b>{{$records->count()}}</b> out of <b>{{$records->total()}}</b> entries</div>
                                {{ $records->links() }}
                            </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Add

        $('#upload_form').on('submit', function(event){
            event.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:"{{ route('ajaxupload.action') }}",
                method:"POST",
                data:formData,
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data)
                {
                    console.log(data.record);
                    $('#table_data').append(data.record);
                    Swal.fire(
                        'Remind!',
                        'Data Added successfully!',
                        'success'
                    );

                },
                error: function(data)
                {
                    console.log(data);
                }
            })
        });

        $(document).on('click', '.edit', function() {
            $tr = $(this).parents('tr');
            $tr.find('.update_btn').removeClass('hidden');
            $tr.find('.deleteData').addClass('hidden');
            $tr.find('.first_name').removeAttr('disabled');
            $tr.find('.last_name').removeAttr('disabled');
            $tr.find('.file-input').prop('type', 'file');
            $tr.find('.edit').text('Cancel');
            $tr.find('.edit').addClass('cancel')
        });

        $(document).on('click', '.cancel', function() {
            $tr = $(this).parents('tr');
            $tr.find('.update_btn').addClass('hidden');
            $tr.find('.deleteData').removeClass('hidden');
            $tr.find('.file-input').prop('type', 'hidden');
            $tr.find('.first_name').attr('disabled','disabled');
            $tr.find('.last_name').attr('disabled','disabled');
            $tr.find('.edit').text('Edit');

        });

        // Update

        $('.update_btn').on('click', function(event){
            event.preventDefault();
            var id = $(this).data('id');
            var form_data = new FormData();
            var first_name = $tr.find("input[name=fname]").val();
            var last_name = $tr.find("input[name=lname]").val();
            $tr = $(this).parents('tr');
            form_data.append('_token','{{csrf_token()}}');
            form_data.append('id',id);
            form_data.append('first_name',first_name);
            form_data.append('last_name',last_name);
            form_data.append('image',$('#image_' + id)[0].files[0]);
            $.ajax({
                url:"{{ route('ajaxupload.update') }}",
                method:"post",
                data:form_data,
                dataType:'json',
                processData: false,
                contentType: false,
                success: function(data)
                {
                    if(data.status == 'success')
                    {
                        console.log(data.record);
                        $("#tr"+id).remove();
                        $('#table_data').append(data.record);
                        $tr = $(this).parents('tr');
                        $tr.find('.update_btn').addClass('hidden');
                        $tr.find('.deleteData').removeClass('hidden');
                        $tr.find('.file-input').prop('type', 'hidden');
                        $tr.find('.first_name').attr('disabled','disabled');
                        $tr.find('.last_name').attr('disabled','disabled');
                        $tr.find('.edit').text('Edit');
                        Swal.fire(
                            'Remind!',
                            'Data Updated successfully!',
                            'success'
                        );


                    }
                    else
                    {
                        console.log('validation_error',data);
                        Swal.fire(
                            'Error!',
                            'Please Insert Data!',
                            'error'
                        );
                    }

                },
                error: function(data)
                {
                    console.log('ajax error',data);
                }
            })
        });

        // Delete

        $(".deleteData").click(function(e){
            if(!confirm("Do you really want to do this?")) {
                return false;
            }
            e.preventDefault();
            var id = $(this).data('id');
            var url = $(this).attr('href');
            $.ajax(
                {
                    url: url,
                    method:"POST",
                    data: {'id': id},
                    dataType:'json',
                    processData: false,
                    contentType: false,
                    success: function (data){
                        console.log(data.record);
                        $("#tr"+id).remove();
                        Swal.fire(
                            'Remind!',
                            'Data deleted successfully!',
                            'success'
                        );

                    }
                });
            return false;
        });
    });
</script>